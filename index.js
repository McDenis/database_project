/********************************************************************/
/*                           REQUIRE                                */
/********************************************************************/

const bodyParser = require('body-parser');
const methodOverride = require('method-override')
const express = require("express")
const app = express()
const ejs = require("ejs")
const path = require("path")
const sqlite3 = require("sqlite3").verbose();

/********************************************************************/
/*                            DATABASE                              */
/********************************************************************/
const db_name = path.join(__dirname, "data", "users.db")
const db = new sqlite3.Database(db_name, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Successful connection to the database 'users.db'")
});

/********************************************************************/
/*                            USE/SET                               */
/********************************************************************/

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(methodOverride('X-HTTP-Method-Override'));
app.use(methodOverride('DELETE'));
app.use(methodOverride('_method'));

app.set("view engine", "ejs")
app.set("views", path.join(__dirname, "views"))

/********************************************************************/
/*                          START PROJECT                           */
/********************************************************************/

app.listen(8000, () => {
  console.log("Server started (http://localhost:8000/) !")
})

//Index, render index view 
app.get("/", (req, res) => {
  res.render("index");
})

//Database, get all data and render database view 
app.get("/database", (req, res) => {
  const sql = "SELECT * FROM users ORDER BY id"
  db.all(sql, [], (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    res.render("database", { model: rows });
  });
});

//About, render about view 
app.get("/about", (req, res) => {
  res.render("about");
});

//GET edit/x and render edit view 
app.get("/edit/:id", (req, res) => {
  const id = req.params.id;
  const sql = "SELECT * FROM users WHERE id = (?)";
  db.get(sql, id, (err, row) => {
    res.render("edit", { model: row });
  });
});

//POST edit/x and render database view (not working) 
app.post("/edit/:id", (req, res) => {
  const id = req.params.id;
  const sql = "UPDATE users SET firstname = (?), lastname = (?), email = (?), born_date = (?), country = (?) WHERE id = (?)";
  const user = [req.body.firstname, req.body.lastname, req.body.email, req.body.born_date, req.body.country, id];
  db.run((sql, user), err => {
    if(err){
      console.log(req.body.firstname);
    }
    res.redirect("/database");
  });
});

//GET /create and render create view
app.get("/create", (req, res) => {
  res.render("create", { model: {} });
});

//POST /create and render database view (not working)
app.post("/create", (req, res) => {
  const sql = "INSERT INTO users (Firstname, Lastname, Email, Born_Date, Country) VALUES (?, ?, ?, ?, ?, ?)";
  const user = [req.body.firstname, req.body.lastname, req.body.email, req.body.born_date, req.body.country];
  db.run(sql, user, err => {
    if (err){
      console.log(err);
    }
    res.redirect("/database");
  });
});

//Delete an user with id
app.get('/delete/:id', (req, res, next) => {
  const sql = "DELETE FROM users WHERE id = (?)";
  const user = req.params.id;
  db.run(sql, user, err => {
    if (err){
      console.log(err);
    }
    res.redirect("/database");
  });
});
