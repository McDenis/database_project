const router = require('express').Router();
const databaseRouter = require('./database/router.js');
router.use('/', databaseRouter);

module.exports = router;